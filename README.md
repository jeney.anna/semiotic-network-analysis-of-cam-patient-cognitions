# NETWORK ANALYSIS OF PATIENT DECISION-MAKING REGARDING CHOICE OF THERAPY
We analyzed semi-structured interview data with Epistemic Network Analysis to explore associations among patients' trusted sources of information, their lay etiology, and milestones in their patient journey. We were interested in how the interplay among these factors differ in patients who use biomedicine only and those who employ non-conventional medicine.

The main analysis document is hosted at GitLab pages at https://matherion.gitlab.io/semiotic-network-analysis-of-cam-patient-cognitions.

